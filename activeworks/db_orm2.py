from tortoise import Tortoise, fields, run_async
from tortoise.models import Model

class Userx(Model):
    id = fields.IntField(pk=True)    # BigIntField, CharField, UUIDField
    name = fields.TextField()

    def __str__(self):
        return self.name
'''
CREATE TABLE IF NOT EXISTS "userx" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "name" TEXT NOT NULL
);

'''


async def run():
    await Tortoise.init(db_url="sqlite://sq1.db", modules={"models": ["__main__"]})
    await Tortoise.generate_schemas()

    user = await Userx.create(name="mws2")

if __name__ == "__main__":
    run_async(run())