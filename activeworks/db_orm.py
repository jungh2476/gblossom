import asyncio
from tortoise import fields
from tortoise.models import Model
from tortoise import Tortoise, run_async





class Userx(Model):
    id = fields.IntField(pk=True)    # BigIntField, CharField, UUIDField
    name = fields.TextField()

db_url1 = 'sqlite://sq1.db'  # :memory: sq1.db 'sqlite://db.sqlite3'
# sqlite3 C:\Users\jungh\webProjects\gblossom\activeworks\sq1.db
# sqlite> ATTACH DATABASE "C:\Users\jungh\webProjects\gblossom\activeworks\sq2.db" AS sq2;
# sqlite> .database
# .tables
# .output FILENAME
# sqlite>.exit
# sqlite3 testDB.db .dump > testDB.sql
# sqlite3 testDB.db < testDB.sql
# sqlite> CREATE TABLE User2( ID INT PRIMARY KEY NOT NULL, NAME TEXT NOT NULL);
# sqlite> .tables
# sqlite> .schema user
# sqlite> select * from user
# sqlite> insert into user(id, name) values (1, 'john dow');
# sqlite> insert into user values (2, 'jane whatson');
# pip install pysqlite3  - normally sqlite included in python 

async def init():
    await Tortoise.init(
        db_url='sqlite://sq1.db',
        modules={'models':['__main__']}  # ['app.models']
    )
    await Tortoise.generate_schemas()
    await Userx.create(name='John Smith2')



async def run():
    await init()
    print('...starting....')
    await Userx.create(name='John Smith2')
    print('....next...')
    user = Userx.filter(name='miss fiver').first()
    #asyncio.run(User2.filter(name='miss fiver').first())
    print(user)
    user = Userx(name="John Smith")
    print(user.name)
    print("inserted")
    


'''
asyncio.run(user.save())
print(user)

async def task1():
    user = await User.create(name='John Smith')
    #equivalent to
    #user1 = User(name='John Smith')
    #await user1.save()
    return user





user_result = asyncio.run(task1())
print(user_result)
'''
if __name__ == "__main__":
    #asyncio.run(init())
    #run_async(init())
    run_async(run())