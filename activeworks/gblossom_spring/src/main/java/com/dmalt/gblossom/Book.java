package com.dmalt.gblossom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
	
	public Book(int i, String string) {
		this.id = i;
		this.nameString=string;
	}
	private int id;
	private String nameString;

}
