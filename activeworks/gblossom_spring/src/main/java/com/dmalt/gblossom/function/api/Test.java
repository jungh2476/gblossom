package com.dmalt.gblossom.function.api;

import java.util.function.Function;

//curl -H "Content-Type: text/plain" localhost:8080/test -d stringtoinject
public class Test implements Function<String, String> {

	@Override
	public String apply(String t) {

		return "Hello World"+t;
	}

}
