package com.dmalt.gblossom;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

//https://www.baeldung.com/spring-cloud-function : case for aws lamda function

@SpringBootApplication
public class GblossomApplication {
	
	static Logger logger = LoggerFactory.getLogger(GblossomApplication.class);
	
	
	public static void main(String[] args) {
		SpringApplication.run(GblossomApplication.class, args);
		logger.info("hello app started:");  //
	}
	
	@Bean
    public Function<String, String> reverseString() {  //Target, Resource R Fnc.apply(T t)
        return value -> new StringBuilder(value).reverse().toString();
    }
	//curl -H "Content-Type: text/plain" localhost:8080/reverseString -d HiMyDarling
	
	@Bean
	public Supplier<Book> getBook(){
		return ()->new Book(101,"Java Book1");
	}
	
	//curl -H "Content-Type: text/json" localhost:8080/getBook -d HiMyDarling
	
	
	@Bean
	public Consumer<String> printMessage(){ //accept and void return
		return(input)->System.out.print(input);
	}
	//curl -H "Content-Type: text/plain" localhost:8080/printMessage -d Welcome-to-my-world
	

}
